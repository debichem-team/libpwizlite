#!/bin/bash

set -e

usage()
{
	printf "\nUsage: $(basename $0) <source relative dir> <dest absolute dir> [ [ --dry-run ] [ --must-exist ] [ --test-deb-package ] ]\n"

	printf "\nThis program will duplicate a source tree directory (arg 1) to a destination tree directory (arg 2).\n"

	printf "\nTo perform a dry run, add --dry-run to the command line.\n"

	printf "\nOptionally, the files are copied from source to destination only if
	the destination file already exists. This process allows one to refresh the
	contents of an existing destination tree with a new copy from a more recent
	source tree. To apply this condition provide a third argument :
	--must-exist.\n"

	printf "\n Optionally, the files can be tested for their shipment by a Debian
	package. To perform this test, provide a fourth argument:
	--test-deb-package.\n"

	printf "\nPlease provide a source directory to find files from\n"
	printf "\nand a destination directory to store the files into.\n\n"

	printf "\nNote that you must be located in the directory that is up one depth
	from the source directory of interest, providing a relative path to it. The
	destination directory might be absolute." 

	printf "A typical invocation might be:\n"
	printf "$0 <relative path source dir> <full path dest dir>\n\n"

	exit 1
}

dry_run=0
no_create_new_file=0
test_deb_package=0

POSITIONAL=()

while [[ $# -gt 0 ]]

do

	key="$1"

	case $key in
		--dry-run)
			dry_run=1
			printf "Performing a dry run.\n"
			shift # past argument
			;;
		--must-exist)
			no_create_new_file=1
			printf "Only refreshing existing files.\n"
			shift # past argument
			;;
		--test-deb-package)
			test_deb_package=1
			printf "Test if Debian package ships the file.\n"
			shift # past argument
			;;
		--default)
			DEFAULT=YES
			shift # past argument
			;;
		*)    # unknown option
			POSITIONAL+=("$1") # save it in an array for later
			printf "One positional argument: $1.\n"
			shift # past argument
			;;
	esac
done

# Restore the position arguments and check that there are 2 !
set -- "${POSITIONAL[@]}" # restore positional parameters

#printf "Argument count: $#\n"

if test $# -gt 2; then

	printf "\nError in the command line. Check it for typos, please.\n"
	usage
fi

if test $# -lt 2; then

	printf "\nThere must be at least two compulsory arguments. Please check your command line.\n"
	usage
fi

if test ! -d $1; then
	printf "\nSource directory does not exist\n"
	exit 1
fi

if test ! -d $2; then
	printf "\nDestination directory does not exist\n"
	exit 1
fi

src_tree_dir=$1
dest_tree_dir=$2


for file in $(find ${src_tree_dir} -type f)
do 

	printf "Working on ${file}... \n"

	dir_name=$(dirname ${file})
	base_name=$(basename ${file})

	# If we only refresh existing files
	if test ${no_create_new_file} -eq 1; then

	# We need to test if the file exists already
	if test -f ${dest_tree_dir}/${file}; then

			# If the file exists, then perform the refresh by copying the file

			if test ${dry_run} -ne 1; then

				# Only if this is not a dry run.

				printf "mkdir -p ${dest_tree_dir}/${dir_name}\n"
				mkdir -p ${dest_tree_dir}/${dir_name}

				printf "cp ${file} ${dest_tree_dir}/${file}\n"
				cp ${file} ${dest_tree_dir}/${file}

			else

<<<<<<< HEAD
				printf "Dry run, should be copying ${file} to ${dest_tree_dir}/${file}.\n"
=======
				printf "Dry run, not copying ${file} to ${dest_tree_dir}/${file}.\n"
>>>>>>> 4b1f6a2 (Reinstate the script.)

			fi

		else

			# The file does not exist: do nothing.
			printf "${file} does not exist in destination tree, skipping.\n"
	fi

else

	# We are NOT only refreshing files, copy anything we find

	if test ${dry_run} -ne 1; then

				# Only if this is not a dry run.

				printf "mkdir -p ${dest_tree_dir}/${dir_name}\n"
				mkdir -p ${dest_tree_dir}/${dir_name}

				printf "cp ${file} ${dest_tree_dir}/${file}\n"
				cp ${file} ${dest_tree_dir}/${file}

	fi

	fi

	if test ${test_deb_package} -eq 1; then
		result=$(apt-file search ${file})

		if test $? -ne 0; then
			printf "==> not provided by any package.\n\n"
		else
			printf "==> provided by the following package(s):\n ${result}\n\n"
		fi
	fi

done

