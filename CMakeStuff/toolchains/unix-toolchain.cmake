message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)


find_package(ZLIB REQUIRED)


find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED )


include(FindHDF5)
find_package(HDF5 COMPONENTS CXX)

#message(STATUS "HDF5_LIBRARIES: ${HDF5_LIBRARIES}")
message(STATUS "HDF5_CXX_LIBRARIES: ${HDF5_CXX_LIBRARIES}")
#message(STATUS "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}")
message(STATUS "HDF5_CXX_INCLUDE_DIRS: ${HDF5_CXX_INCLUDE_DIRS}")

# On GNU/Linux, we can use mzMLb.
# add_definitions(-DWITHOUT_MZMLB)

add_definitions(-fPIC)

# Install cmake module
install(FILES ${CMAKE_MODULE_PATH}/FindPwizLite.cmake
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/pwizlite)

# Install cmake config
configure_file (${CMAKE_MODULE_PATH}/PwizLiteConfig.cmake.in
	${CMAKE_BINARY_DIR}/PwizLiteConfig.cmake)
install(FILES ${CMAKE_BINARY_DIR}/PwizLiteConfig.cmake
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/pwizlite)

# Install the PkgConfig config file
configure_file (${CMAKE_MODULE_PATH}/pkgconfig/libpwizlite.pc.in
	${CMAKE_BINARY_DIR}/libpwizlite.pc)
install(FILES ${CMAKE_BINARY_DIR}/libpwizlite.pc
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)


