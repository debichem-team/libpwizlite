# https://github.com/TRIQS/triqs/issues/523
add_definitions(-D_LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR)

# Only for Apple macport
set(CMAKE_MACOSX_RPATH 0)

