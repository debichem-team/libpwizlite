message("\n${BoldRed}WIN10-MINGW64 environment${ColourReset}\n")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/bin")


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 or _WIN32 defined and WIN10MINGW64 defined also.")
endif()


find_package(ZLIB REQUIRED)


find_package(Boost COMPONENTS iostreams thread filesystem chrono REQUIRED )

# Sadly, this configuration makes the pwizlite link with the static
# library, which we want to avoid so that the behaviour is identical
# to what happens on GNU/Linux.
# include(FindHDF5)
# find_package(HDF5 COMPONENTS CXX)

#message(STATUS "HDF5_LIBRARIES: ${HDF5_LIBRARIES}")
message(STATUS "HDF5_CXX_LIBRARIES: ${HDF5_CXX_LIBRARIES}")
#message(STATUS "HDF5_INCLUDE_DIRS: ${HDF5_INCLUDE_DIRS}")
message(STATUS "HDF5_CXX_INCLUDE_DIRS: ${HDF5_CXX_INCLUDE_DIRS}")


set(hdf5_FOUND 1)
set(hdf5_INCLUDE_DIRS "C:/msys64/ucrt64/include")
set(hdf5_LIBRARIES "C:/msys64/ucrt64/bin/libhdf5_cpp-310.dll")
if(NOT TARGET hdf5::hdf5_cpp)
	add_library(hdf5::hdf5_cpp UNKNOWN IMPORTED)
	set_target_properties(hdf5::hdf5_cpp PROPERTIES
		IMPORTED_LOCATION             "${hdf5_LIBRARIES}"
		INTERFACE_INCLUDE_DIRECTORIES "${hdf5_INCLUDE_DIRS}"
		)
endif()

# F. Rusconi
# At the moment, mzMLb does not work on Win10 because the
# link step fails with tons of undefined references to libhdf5.
# I tried hard but failed to make that work.
#
# So the procedure as follows:
#
set(hdf5_LIBRARIES "")
set(hdf5_INCLUDE_DIRS "")
add_definitions(-DWITHOUT_MZMLB)
#
# Move pwiz/data/msdata/IO-FRusconi-removed-all-mzmlb-related-code.cpp
# to
# pwiz/data/msdata/IO.cpp
#
# In the src/CMakeLists.txt, comment out file
# pwiz/data/msdata/mzmlb/Connection_mzMLb.cpp
# from the list of source files.

# see https://cmake.org/pipermail/cmake/2015-December/062166.html
set(CMAKE_NO_SYSTEM_FROM_IMPORTED 1)

# Specific library to link to:
message(STATUS "Add psapi.dll as a win64-specific link-time dependency.")
list(APPEND PLATFORM_SPECIFIC_LINK_LIBRARIES "psapi")

# On Win10 all the code is relocatable.
remove_definitions(-fPIC)

# Install cmake module
install(FILES ${CMAKE_MODULE_PATH}/FindPwizLite.cmake
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/pwizlite)

# Install cmake config
configure_file (${CMAKE_MODULE_PATH}/PwizLiteConfig.cmake.in
	${CMAKE_BINARY_DIR}/PwizLiteConfig.cmake)
install(FILES ${CMAKE_BINARY_DIR}/PwizLiteConfig.cmake
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/pwizlite)

# Install the PkgConfig config file
configure_file (${CMAKE_MODULE_PATH}/pkgconfig/libpwizlite.pc.in
	${CMAKE_BINARY_DIR}/libpwizlite.pc)
install(FILES ${CMAKE_BINARY_DIR}/libpwizlite.pc
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)


