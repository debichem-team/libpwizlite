# Packager

# Note how the doc/user-manual/DC-user-manual is listed here:
# this is a generated file (configure_file) that needs not be
# in the tarball. However, because it is a configure_file it is
# generated right while doing 'cmake archive', so it is not
# possible that it be absent from source dir!set(CPACK_PACKAGE_NAME "massxpert2")
set(CPACK_PACKAGE_NAME ${CMAKE_PROJECT_NAME})
set(CPACK_PACKAGE_VENDOR "msXpertSuite")
set(CPACK_PACKAGE_VERSION "${PWIZLITE_VERSION}")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Dumbed-down library from the ProteoWizard libpwiz source code for use with libpappsomspp")
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_SOURCE_IGNORE_FILES
  ".git/"
  ".gitattributes"

  "CMakeStuff/toolchains/mxe-toolchain.cmake"

  ".cache"
  "compile_commands.json"

  ${CPACK_PACKAGE_NAME}.kdev4
  ".kdev4"
  "development.kdev4"
  "Session.vim"
  "debian"

  ${CPACK_SOURCE_IGNORE_FILES}
)

set(CPACK_SOURCE_PACKAGE_FILE_NAME
  "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")

include(CPack)


