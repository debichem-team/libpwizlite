# Copyright : Filippo Rusconi
# License : GPL-3.0+
# Authors : Filippo Rusconi

find_path(PwizLite_INCLUDE_DIRS pwizlite/pwiz/utility/misc/Export.hpp
	PATHS /usr/local/include /usr/include
	PATH_SUFFIXES pwizlite libpwizlite ENV PATH)


find_library(PwizLite_LIBRARY NAMES pwizlite)

if(PwizLite_INCLUDE_DIRS AND PwizLite_LIBRARY)

	set(PwizLite_FOUND TRUE)

endif()

if(PwizLite_FOUND)

	if(NOT PwizLite_FIND_QUIETLY)
		message(STATUS "Found PwizLite_LIBRARY: ${PwizLite_LIBRARY}")
	endif()

	if(NOT TARGET PwizLite::PwizLite)

		add_library(PwizLite::PwizLite UNKNOWN IMPORTED)

		set_target_properties(PwizLite::PwizLite PROPERTIES
			IMPORTED_LOCATION             "${PwizLite_LIBRARY}"
			INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}")

	endif()

endif()
