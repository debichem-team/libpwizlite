============
 pwizlite
============

-----------------------------------
Custom reduced Proteowizard library
-----------------------------------

:Author: Filippo Rusconi <lopippo@debian.org>
:Date: 20200601  
:Copyright: Filippo Rusconi
:Version: 1
:Manual section: 3
:Manual group: pwizlite


SYNOPSIS
========

pwizlite is a shared object library (\*.so file)


DESCRIPTION
===========

The pwizlite shared object library is a reduced copy of the Proteowizard
libpwiz library. The pwizlite library only contains code that allows the
user to load XML-based standard mass spectrometry formats: mzXML and mzML
and MGF files.

Rationale: The Proteowizard project uses the Boost BJam build system to build
their library and binary images. The source tree contains all the libraries
that libpwiz depends upon. The configuration of BJam is so complicated 
that making the build system use the system-based libraries is terribly 
difficult. The Proteowizard project used to maintain an autotools
build system port for GNU/Linux users. But that has been discontinued.

Many projects (and mine also) have resorted to using a reduced set of libpwiz
source files only directed at building the XML-based and MGF format file-loading
capabilities. In my packaging experience, at least one package makes use of such
a subset copy of libpwiz. Having a package like pwizlite in Debian will ease
packaging of all the other packages.


EXAMPLES
========

* To include files from the pwizlite library, write an inclusion statement of
  this sort:

  #include <pwizlite/pwiz/<path>/<to>/<file.h>

* To link to the pwizlite library add a linker flag: -lpwizlite.

* To CMake-find the package: find_package(PwizLite)

* To CMake-configure the package: find_package(PwizLiteConfig.cmake)

* To use the pkgconfig system: 
  pkg-config --help 
  pkg-config --libs pwizlite


MAN PAGE PRODUCTION
===================

This man page was written by Filippo Rusconi <lopippo@debian.org>. 
